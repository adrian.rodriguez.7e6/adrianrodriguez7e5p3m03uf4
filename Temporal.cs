﻿using System;
using System.Runtime.CompilerServices;

namespace Temporal
{
    internal class Program
    {
        public class Data
        {
            public int dia { get; set; }
            public int mes { get; set; }
            public int anyo { get; set; }

            public Data()
            {
                this.dia = 1;
                this.mes = 1;
                this.anyo = 1980;
            }

            public Data(int Dia, int Mes, int Anyo)
            {
                this.dia = Dia;
                this.mes = Mes;
                this.anyo = Anyo;

                if (Anyo >= 1980 && Anyo <= 2023)
                {
                    if (Mes >= 1 && Mes <= 12)
                    {
                        if (Dia > 0 && Dia <= 31)
                        {
                            this.dia = Dia;
                            this.mes = Mes;
                            this.anyo = Anyo;
                        }
                        else
                        {
                            new Data();
                        }
                    }
                    else
                    {
                        new Data();
                    }
                }
                else
                {
                    new Data();
                }
            }

            public Data(Data obj)
            {
                this.dia = obj.dia;
                this.mes = obj.mes;
                this.anyo = obj.anyo;
            }

            public int SumarRestarDias(int operacion)
            {
                if (this.dia + operacion<30 && this.dia + operacion >1) { this.dia += operacion; }
                else if (this.dia + operacion > 30) { this.dia = operacion - (30 - this.dia); }
                else if (this.dia +operacion<1) { this.dia = (this.dia+operacion)*(-1); }
                return this.dia;
            }

            public int SepareDays(Data objeto1, Data objeto2)
            {
                int separacion = 0;
                if (objeto1.dia > objeto2.dia) { separacion = (30 - objeto1.dia) + objeto2.dia; }
                else separacion = objeto2.dia-objeto1.dia;
                return separacion;
            }

            public string CompareDays(Data objeto1, Data objeto2)
            {
                if (objeto1.anyo > objeto2.anyo) { return "La fecha 1 es la mayor"; }
                else if (objeto1.anyo < objeto2.anyo) { return "La fecha 2 es la mayor"; }
                else
                {
                    if (objeto1.mes > objeto2.mes) { return "La fecha 1 es la mayor"; }
                    else if (objeto1.mes < objeto2.mes) { return "La fecha 2 es la mayor"; }
                    else
                    {
                        if (objeto1.dia > objeto2.dia) { return "La fecha 1 es la mayor"; }
                        else if (objeto1.dia < objeto2.dia) { return "La fecha 2 es la mayor"; }
                        else { return "Las fechas son iguales"; }
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            Data fechaDefault = new Data();
            Console.WriteLine($"Fecha default:\n\tDia: {fechaDefault.dia}\n\tMes: {fechaDefault.mes}\n\tAnyo: {fechaDefault.anyo}");

            Data fechaDeObjeto = new Data(fechaDefault);
            Console.WriteLine($"Fecha default:\n\tDia: {fechaDeObjeto.dia}\n\tMes: {fechaDeObjeto.mes}\n\tAnyo: {fechaDeObjeto.anyo}");

            Data fechaConParametros = new Data(6,2,2003);
            Console.WriteLine($"Fecha default:\n\tDia: {fechaConParametros.dia}\n\tMes: {fechaConParametros.mes}\n\tAnyo: {fechaConParametros.anyo}");

            fechaConParametros.dia = fechaConParametros.SumarRestarDias(+5);
            Console.WriteLine(fechaConParametros.dia);
            fechaConParametros.dia = fechaConParametros.SumarRestarDias(-15);
            Console.WriteLine(fechaConParametros.dia);
            fechaConParametros.dia = fechaConParametros.SumarRestarDias(+50);
            Console.WriteLine(fechaConParametros.dia);

            Console.WriteLine(fechaConParametros.CompareDays(fechaDeObjeto, fechaConParametros));

            Console.WriteLine(fechaConParametros.SepareDays(fechaDeObjeto, fechaConParametros));
        }
    }
}
